def distance(a_word: str, b_word: str) -> bool:
    return len(a_word) - sum(int(a_ch == b_ch) for a_ch, b_ch in zip(a_word, b_word))

def are_neighbours(a_word: str, b_word: str) -> bool:
    return distance(a_word, b_word) == 1

def neighbours(a_word: str, all_words: list[str]) -> list[str]:
    return [w for w in all_words if are_neighbours(w, a_word)]

def is_closer(start_word: str, curr_word: str, end_word: str) -> bool:
    return distance(curr_word, end_word) <= distance(start_word, end_word)

def can_reach(a_word: str, b_word: str, all_words: str, path: list[str] = []) -> list[str]:
    adjacents = neighbours(a_word, [w for w in all_words if w not in path])
    adjacents = sorted(adjacents, key=lambda word: distance(word, b_word))
    if len(adjacents) == 0:
        return []
    if b_word in adjacents:
        #print(path)
        return path
    for adj in adjacents:
        if is_closer(a_word, adj, b_word):
            if p := can_reach(adj, b_word, all_words, path + [adj]):
                return p
    return []
    
def reorder(all_words: list[str], a_word: str, b_word: str) -> list[str]:
    def cost(word, a_word, b_word):
        return distance(word, a_word) + distance(word, b_word)
    return list(sorted(all_words, key=lambda w: cost(w, a_word, b_word)))

def path(a_word: str, b_word: str) -> list[str]:
    all_words = [word.strip() for word in open('sowpods.txt', 'r') if len(word.strip()) == len(a_word)]
    all_words = reorder(all_words, a_word, b_word)
    return can_reach(a_word, b_word, all_words)    

print(path("ABOUT", "HOUSE"))